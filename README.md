# Noclist

## Requirements
In order to run this script you need to install the dependencies.

```
pip3 install -r requirements.txt
```

## Start the dockerized api 

```
docker run --rm -p 8888:8888 adhocteam/noclist
```

## Before run
Be sure that the script has execution rights or give it to it

```
chmod +x noclist.py
```

## Run
By default the script will try to connect to 'localhost:8888', if you want to change
that you can define a new environment variable API_URL pointing whatever you need

Example 1 (normal run to localhost:8888)
```
./noclist.py
```

Example 2 (defining an environment variable)
```
export API_URL=http://my.api.com:8888
./noclist.py
```