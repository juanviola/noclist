#!/usr/bin/env python3

import requests
from requests.adapters import HTTPAdapter
import sys
import hashlib
import json
from time import sleep
from os import getenv

API_URL = getenv('API_URL', 'http://localhost:8888')

def doReq(url, method = 'GET', headers = {}, data = {}, retries = 3):
  '''
  This function do a request against the url and if it fails it will
  retry the amount of times specified on retries parameter. If the total
  of retries is reached it will exit with a non cero value 

  Args:
    url:      (string) entpoint to do the request
    method:   (string) GET || POST
    headers:  (dict) object with headers
    data:     (dict) data to do a post request
    retries:  (int) number of retries

  Return:
    [on successfull] {object} requests object
    [on error] it will exit the program with number 100
  '''
  loop = 0
  while loop < retries:
    try:
      req = requests.get(url, headers=headers, data=data) if method == 'GET' else requests.post(url, headers=headers, data=data)
      if req.status_code != 200:
        raise Exception('not-200')
      return req
    except requests.exceptions.RequestException as e:
      pass
    
    loop+=1
    sleep(2)

  sys.exit(100)


def getToken():
  '''
  This function gets an authentication token

  Return:
    [on successfull] {string} header badsec-authentication-token
    [on error] it will exit the program with number 98 
  '''
  try:
    req = doReq(API_URL + '/auth')
    return req.headers['badsec-authentication-token']
  except Exception:
    pass 
    sys.exit(98)

def getUsers():
  '''
  This function do a request against the users endpoint sending the token as
  a hashed sha256 header

  Return:
    [on successfull] {array} outputs an json array with all the values
    [on error] it will exit the program with number 99 
  '''

  try:
    token = getToken()
    tokenHash = hashlib.sha256()
    tokenHash.update("{0}/users".format(token).encode('utf-8'))
    tokenChecksum = tokenHash.hexdigest()

    req = doReq(API_URL + '/users', headers={ 'X-Request-Checksum': tokenChecksum })
    data = [line.strip() for line in req.text.splitlines()] 
    print (json.dumps(data))
  except Exception:
    pass
    sys.exit(99)

if __name__ == '__main__':
  getUsers()
